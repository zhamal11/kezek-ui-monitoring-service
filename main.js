Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Пуск газа',
            'ПНР (Врезка, Приемка в эксп-ю,<br> Опломбировка ПУ, Первичный пуск газа)',
            'Рабочие проекты/ИТД',
            'Опломбировка'
        ],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: '',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' '
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Всего',
        data: [17, 31, 23, 2],
        color: '#7FB800'
    }, {
        name: 'Отложенные',
        data: [13, 16, 8, 6],
        color: '#2755BD'
    }, {
        name: 'Клиент не пришел',
        data: [4, 12, 14, 27],
        color: '#B22178'
    }, {
        name: 'Обслужено',
        data: [16, 11, 26, 4],
        color: '#F25700'
    },
        {
            name: 'Бронирование',
            data: [9, 13, 12, 14],
            color: '#19A5E1'
        },
        {
            name: 'Ожидающие',
            data: [16, 4, 26, 14],
            color: '#FEB701'
        }]
});
