
Highcharts.chart('container2', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: [
            'Пуск газа',
            'ПНР (Врезка, Приемка в эксп-ю, Опломбировка ПУ, Первичный пуск газа)',
            'Рабочие проекты/ИТД',
            'Опломбировка'
        ]
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: [{
        name: 'Всего',
        data: [5, 3, 4, 7],
        color: '#7FB800'
    }, {
        name: 'Отложенные',
        data: [2, 2, 3, 2],
        color: '#2755BD'
    }, {
        name: 'Клиент не пришел',
        data: [3, 4, 4, 2],
        color: '#B22178'
    },
        {
            name: 'Обслужено',
            data: [5, 3, 4, 7],
            color: '#F25700'
        }, {
            name: 'Бронирование',
            data: [2, 2, 3, 2],
            color: '#19A5E1'
        }, {
            name: 'Ожидающие',
            data: [3, 4, 4, 5],
            color: '#FEB701'
        }
    ]
})